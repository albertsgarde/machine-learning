mod nodes;
mod structure;

pub use nodes::*;
pub use structure::*;

#[cfg(test)]
mod test {
    use crate::{
        comp_net::{OperatorAdd, OperatorMultiply},
        Matrix,
    };

    use super::*;

    #[test]
    fn simple_addition() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let z = builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
        let mut net = builder.build();
        assert_eq!(net.source_value(x), &Matrix::from_diag_elem(1, 3.));
        assert_eq!(net.source_value(y), &Matrix::from_diag_elem(1, 6.));
        assert_eq!(net.operator_value(z), None);
        assert_eq!(net.node_value(x), Some(&Matrix::from_diag_elem(1, 3.)));
        assert_eq!(net.node_value(y), Some(&Matrix::from_diag_elem(1, 6.)));
        assert_eq!(net.node_value(z), None);
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
    }

    #[test]
    fn two_layer_addition() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let z = builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
        let w = builder.add_operator(OperatorAdd, vec![z.as_any(), y.as_any()]);
        let mut net = builder.build();
        assert_eq!(net.source_value(x), &Matrix::from_diag_elem(1, 3.));
        assert_eq!(net.source_value(y), &Matrix::from_diag_elem(1, 6.));
        assert_eq!(net.operator_value(z), None);
        assert_eq!(net.node_value(x), Some(&Matrix::from_diag_elem(1, 3.)));
        assert_eq!(net.node_value(y), Some(&Matrix::from_diag_elem(1, 6.)));
        assert_eq!(net.node_value(z), None);
        assert_eq!(net.operator_value(w), None);
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
        assert_eq!(net.operator_value(w), None);
        net.compute(w);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
        assert_eq!(net.operator_value(w), Some(&Matrix::from_diag_elem(1, 15.)));
    }

    #[test]
    fn two_layer_addition_indirect_computation() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let z = builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
        let w = builder.add_operator(OperatorAdd, vec![z.as_any(), y.as_any()]);
        let mut net = builder.build();
        assert_eq!(net.source_value(x), &Matrix::from_diag_elem(1, 3.));
        assert_eq!(net.source_value(y), &Matrix::from_diag_elem(1, 6.));
        assert_eq!(net.operator_value(z), None);
        assert_eq!(net.operator_value(w), None);
        net.compute(w);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
        assert_eq!(net.operator_value(w), Some(&Matrix::from_diag_elem(1, 15.)));
    }

    #[test]
    #[should_panic]
    fn illegal_dims() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(2, 6.));
        builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
    }

    #[test]
    #[should_panic]
    fn too_many_inputs() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let w = builder.add_source(Matrix::from_diag_elem(1, 6.));
        builder.add_operator(OperatorMultiply, vec![x.as_any(), y.as_any(), w.as_any()]);
    }

    #[test]
    #[should_panic]
    fn too_few_inputs() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        builder.add_operator(OperatorMultiply, vec![x.as_any()]);
    }

    #[test]
    fn reuse_operator_node() {
        let mut builder = CompNetBuilder::new();
        let a = builder.add_source_with_name(Matrix::from_elem((1, 2), 6.), "a");
        let b = builder.add_source_with_name(Matrix::from_elem((1, 2), 2.), "b");
        let x = builder.add_operator_with_name(OperatorAdd, vec![a.as_any(), b.as_any()], "x");
        let y = builder.add_operator_with_name(OperatorAdd, vec![x.as_any(), b.as_any()], "y");
        let z = builder.add_operator_with_name(OperatorAdd, vec![x.as_any(), y.as_any()], "z");

        let mut net = builder.clone().build();
        assert_eq!(net.compute(z), Matrix::from_elem((1, 2), 2. * 6. + 3. * 2.));
    }

    #[test]
    fn derivative() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let m = builder.add_operator(OperatorMultiply, vec![x.as_any(), a.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![b.as_any(), m.as_any()]);
        let mut net = builder.clone().build();
        net.compute(y);
        assert_eq!(net.operator_value(y), Some(&Matrix::from_diag_elem(1, 20.)));

        let dydm = builder.add_derivative(y, (0, 0), 1);
        let mut net = builder.clone().build();
        net.compute(dydm);
        assert_eq!(
            net.operator_value(dydm),
            Some(&Matrix::from_diag_elem(1, 1.))
        );
    }

    #[test]
    fn differentiate_single_single_derivative() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let m = builder.add_operator(OperatorMultiply, vec![x.as_any(), a.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![b.as_any(), m.as_any()]);
        let mut net = builder.clone().build();
        net.compute(y);
        assert_eq!(net.operator_value(y), Some(&Matrix::from_diag_elem(1, 20.)));

        let dydm = builder.differentiate_single(y, (0, 0), m.as_any());
        let mut net = builder.clone().build();
        net.compute(dydm);
        assert_eq!(
            net.operator_value(dydm),
            Some(&Matrix::from_diag_elem(1, 1.))
        );
    }

    #[test]
    fn chain_derivative() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let m = builder.add_operator(OperatorMultiply, vec![x.as_any(), a.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![b.as_any(), m.as_any()]);
        let mut net = builder.clone().build();
        net.compute(y);
        assert_eq!(net.operator_value(y), Some(&Matrix::from_diag_elem(1, 20.)));

        let dydm = builder.add_derivative(y, (0, 0), 1);
        let mut net = builder.clone().build();
        net.compute(dydm);
        assert_eq!(
            net.operator_value(dydm),
            Some(&Matrix::from_diag_elem(1, 1.))
        );

        let dydx = builder.chain_derivative(dydm, 0);
        let mut net = builder.clone().build();
        net.compute(dydx);
        assert_eq!(
            net.operator_value(dydx),
            Some(&Matrix::from_diag_elem(1, 6.))
        );
    }

    #[test]
    fn differentiate_single_chain_derivative() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let m = builder.add_operator(OperatorMultiply, vec![x.as_any(), a.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![b.as_any(), m.as_any()]);
        let mut net = builder.clone().build();
        net.compute(y);
        assert_eq!(net.operator_value(y), Some(&Matrix::from_diag_elem(1, 20.)));

        let dydx = builder.differentiate_single(y, (0, 0), x.as_any());
        let mut net = builder.clone().build();
        net.compute(dydx);
        assert_eq!(
            net.operator_value(dydx),
            Some(&Matrix::from_diag_elem(1, 6.))
        );
    }

    #[test]
    fn differentiate_single_with_multiple_paths() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let u = builder.add_operator(OperatorAdd, vec![x.as_any(), a.as_any()]);
        let v = builder.add_operator(OperatorAdd, vec![x.as_any(), b.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![u.as_any(), v.as_any()]);

        let dydx = builder.differentiate_single(y, (0, 0), x.as_any());
        let mut net = builder.build();

        let values = vec![(1., 2., 3.), (-2., 4., 8.), (0., 0., 0.)];
        for (x_value, a_value, b_value) in values {
            net.set_value(x, Matrix::from_elem((1, 1), x_value));
            net.set_value(a, Matrix::from_elem((1, 1), a_value));
            net.set_value(b, Matrix::from_elem((1, 1), b_value));

            let y_value = net.compute(y)[(0, 0)];
            let dydx_value = net.compute(dydx)[(0, 0)];

            assert_eq!(y_value, (a_value + x_value) + (b_value + x_value));
            assert_eq!(dydx_value, 2.);
        }
    }

    #[test]
    fn differentiate_single_with_name() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let u = builder.add_operator(OperatorAdd, vec![x.as_any(), a.as_any()]);
        let v = builder.add_operator(OperatorAdd, vec![x.as_any(), b.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![u.as_any(), v.as_any()]);

        let dydx = builder.differentiate_single_with_name(y, (0, 0), x.as_any(), "test_name");

        assert_eq!(builder.node_name(dydx), Some("test_name"))
    }

    #[test]
    fn differentiate_many() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let a = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let b = builder.add_source(Matrix::from_diag_elem(1, 2.));
        let u = builder.add_operator(OperatorAdd, vec![x.as_any(), a.as_any()]);
        let v = builder.add_operator(OperatorAdd, vec![x.as_any(), b.as_any()]);
        let y = builder.add_operator(OperatorAdd, vec![u.as_any(), v.as_any()]);

        let derivatives = builder.differentiate_with_names(
            y,
            (0, 0),
            vec![(x, Some("dy/dx")), (a, Some("dy/da")), (b, Some("dy/db"))],
        );
        let mut net = builder.build();

        assert_eq!(net.compute(derivatives[0]), Matrix::from_elem((1, 1), 2.));
        assert_eq!(net.compute(derivatives[1]), Matrix::from_elem((1, 1), 1.));
        assert_eq!(net.compute(derivatives[2]), Matrix::from_elem((1, 1), 1.));
    }

    #[test]
    fn clear_value() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let z = builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
        let mut net = builder.build();
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
        net.clear_value(z);
        assert_eq!(net.operator_value(z), None);
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
    }

    #[test]
    fn set_value() {
        let mut builder = CompNetBuilder::new();
        let x = builder.add_source(Matrix::from_diag_elem(1, 3.));
        let y = builder.add_source(Matrix::from_diag_elem(1, 6.));
        let z = builder.add_operator(OperatorAdd, vec![x.as_any(), y.as_any()]);
        let mut net = builder.build();
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 9.)));
        net.set_value(x, Matrix::from_elem((1, 1), 1.));
        assert_eq!(net.operator_value(z), None);
        net.compute(z);
        assert_eq!(net.operator_value(z), Some(&Matrix::from_diag_elem(1, 7.)));
    }

    #[test]
    fn add_subnet() {
        let mut builder = CompNetBuilder::new();
        let a = builder.add_source_with_name(Matrix::from_elem((1, 2), 6.), "a");
        let b = builder.add_source_with_name(Matrix::from_elem((1, 2), 2.), "b");
        let x = builder.add_operator_with_name(OperatorAdd, vec![a.as_any(), b.as_any()], "x");

        let mut subnet = CompNetBuilder::new();
        let x_ = subnet.add_source_with_name(Matrix::from_elem((1, 2), 3.), "x'");
        let b_ = subnet.add_source_with_name(Matrix::from_elem((1, 2), 3.), "b'");
        let y = subnet.add_operator_with_name(OperatorAdd, vec![x_.as_any(), b_.as_any()], "y");
        let _ = subnet.add_operator_with_name(OperatorAdd, vec![x_.as_any(), y.as_any()], "z");

        let (_, new_operator_refs) =
            builder.add_comp_net(subnet, vec![Some(x.as_any()), Some(b.as_any())]);

        let mut net = builder.clone().build();
        assert_eq!(
            net.compute(*new_operator_refs.last().unwrap()),
            Matrix::from_elem((1, 2), 18.)
        );
    }
}
