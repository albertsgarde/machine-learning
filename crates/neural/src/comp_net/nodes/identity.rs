use crate::{
    comp_net::{
        CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorConstant,
        OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorIdentity;

impl CompNetNodeFuncs for OperatorIdentity {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self)
    }

    fn name(&self) -> &'static str {
        "identity"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() == 1 {
            Ok(input_dims[0])
        } else {
            Err(InvalidInputDimsError::new(
                "Takes exactly one input.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 1);
        inputs[0].clone()
    }

    fn derivative(
        &self,
        index: (usize, usize),
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert!(index.0 < self_ref.dims.0);
        assert!(index.1 < self_ref.dims.1);
        assert_eq!(inputs.len(), 1);
        assert!(wrt_child < inputs.len());
        let mut value = Matrix::zeros(self_ref.dims);
        value[index] = 1.0;
        Some(super::funcs_to_subnet(OperatorConstant::new(value), vec![]))
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<crate::comp_net::NodeRefAny>)> {
        assert_eq!(inputs.len(), 1);
        assert!(wrt_child < inputs.len());
        Some(super::funcs_to_subnet(
            OperatorIdentity,
            vec![prev_derivative.as_any()],
        ))
    }
}
