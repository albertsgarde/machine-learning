use crate::comp_net::{
    CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorRef, RefDims,
};

pub struct OperatorMultiply;

impl CompNetNodeFuncs for OperatorMultiply {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorMultiply)
    }

    fn name(&self) -> &'static str {
        "multiply"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() != 2 {
            Err(InvalidInputDimsError::new(
                "Takes exactly 2 inputs.",
                input_dims,
                self.name(),
            ))
        } else if input_dims[0] == input_dims[1] {
            Ok(input_dims[0])
        } else {
            Err(InvalidInputDimsError::new(
                "To multiply two matrices elementwise, their dimensions must match.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 2);
        assert_eq!(inputs[0].dim(), inputs[1].dim());
        inputs[0] * inputs[1]
    }

    fn derivative(
        &self,
        _index: (usize, usize),
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        None
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(inputs.len(), 2);
        assert!(wrt_child < inputs.len());
        Some(super::funcs_to_subnet(
            OperatorMultiply,
            vec![inputs[1 - wrt_child], prev_derivative.as_any()],
        ))
    }
}
