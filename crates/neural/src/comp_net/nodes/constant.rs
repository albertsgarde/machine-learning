use crate::{
    comp_net::{
        CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorConstant {
    value: Matrix,
}

impl OperatorConstant {
    pub fn new(value: Matrix) -> Self {
        Self { value }
    }
}

impl CompNetNodeFuncs for OperatorConstant {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self {
            value: self.value.clone(),
        })
    }

    fn name(&self) -> &'static str {
        "constant"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.is_empty() {
            Ok(self.value.dim())
        } else {
            Err(InvalidInputDimsError::new(
                "Takes no inputs.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 0);
        self.value.clone()
    }

    fn derivative(
        &self,
        _index: (usize, usize),
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _inputs: &[RefDims<crate::comp_net::NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<crate::comp_net::NodeRefAny>)> {
        panic!("Derivative cannot be taken of node with no children.")
    }

    fn chain_derivative(
        &self,
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _prev_derivative: RefDims<OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        panic!("Derivative cannot be taken of node with no children.")
    }
}
