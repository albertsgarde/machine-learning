use crate::{
    comp_net::{
        nodes::funcs_to_subnet, CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError,
        NodeRefAny, OperatorConstant, OperatorIdentity, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorAdd;

impl CompNetNodeFuncs for OperatorAdd {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self)
    }

    fn name(&self) -> &'static str {
        "add"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.is_empty() {
            Err(InvalidInputDimsError::new(
                "Takes at least one input.",
                input_dims,
                self.name(),
            ))
        } else if input_dims.windows(2).all(|w| w[0] == w[1]) {
            Ok(input_dims[0])
        } else {
            Err(InvalidInputDimsError::new(
                "To add matrices, their dimensions must match.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert!(!inputs.is_empty());
        assert!(inputs.windows(2).all(|w| w[0].dim() == w[1].dim()));
        let mut result = inputs[0].clone();
        for &input in inputs.iter().skip(1) {
            result += input;
        }
        result
    }

    fn derivative(
        &self,
        index: (usize, usize),
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<crate::comp_net::NodeRefAny>)> {
        assert!(index.0 < self_ref.dims.0);
        assert!(index.1 < self_ref.dims.1);
        assert!(!inputs.is_empty());
        assert!(wrt_child < inputs.len());
        let mut value = Matrix::zeros(self_ref.dims);
        value[index] = 1.0;
        Some(funcs_to_subnet(OperatorConstant::new(value), vec![]))
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert!(!inputs.is_empty());
        assert!(wrt_child < inputs.len());
        Some(funcs_to_subnet(
            OperatorIdentity,
            vec![prev_derivative.as_any()],
        ))
    }
}
