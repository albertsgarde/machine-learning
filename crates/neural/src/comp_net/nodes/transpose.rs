use crate::comp_net::{
    CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorRef, RefDims,
};

pub struct OperatorTranspose;

impl CompNetNodeFuncs for OperatorTranspose {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorTranspose)
    }

    fn name(&self) -> &'static str {
        "transpose"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() != 1 {
            Err(InvalidInputDimsError::new(
                "Transpose takes exactly 1 input.",
                input_dims,
                self.name(),
            ))
        } else if input_dims[0].0 == 0 || input_dims[0].1 == 0 {
            Err(InvalidInputDimsError::new(
                "Input dimensions may not be 0.",
                input_dims,
                self.name(),
            ))
        } else {
            Ok((input_dims[0].1, input_dims[0].0))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 1);
        assert!(inputs[0].dim().0 != 0 && inputs[0].dim().1 != 0);
        inputs[0].t().to_owned()
    }

    fn derivative(
        &self,
        _index: (usize, usize),
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        None
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(wrt_child, 0);
        assert_eq!(inputs.len(), 1);
        assert_eq!(inputs[0].dims, (self_ref.dims.1, self_ref.dims.0));

        Some(super::funcs_to_subnet(
            OperatorTranspose,
            vec![prev_derivative.as_any()],
        ))
    }
}
