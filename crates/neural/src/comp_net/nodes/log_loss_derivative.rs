use ndarray::Zip;

use crate::{
    comp_net::{CompNetNodeFuncs, InvalidInputDimsError},
    Matrix,
};

pub struct OperatorLogLossDerivative;

impl CompNetNodeFuncs for OperatorLogLossDerivative {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorLogLossDerivative)
    }

    fn name(&self) -> &'static str {
        "log_loss_derivative"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() != 2 {
            Err(InvalidInputDimsError::new(
                "Log loss derivative operator takes exactly two inputs.",
                input_dims,
                self.name(),
            ))
        } else if input_dims[0] != input_dims[1] {
            Err(InvalidInputDimsError::new(
                "Log loss derivative takes inputs of the same size.",
                input_dims,
                self.name(),
            ))
        } else {
            Ok(input_dims[0])
        }
    }

    fn compute(&self, inputs: &[&Matrix]) -> Matrix {
        assert_eq!(inputs.len(), 2);
        assert_eq!(inputs[0].dim(), inputs[1].dim());
        let mut result = Matrix::zeros(inputs[0].dim());

        Zip::from(&mut result)
            .and(inputs[0])
            .and(inputs[1])
            .for_each(|result, &y, &estimated_y| {
                *result = (y - estimated_y) / (estimated_y * (1. - estimated_y));
            });
        result
    }
}
