use crate::{
    comp_net::{
        self, CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRef, NodeRefAny,
        OperatorTranspose, RefDims,
    },
    Matrix,
};

pub struct OperatorDotProduct;

impl CompNetNodeFuncs for OperatorDotProduct {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorDotProduct)
    }

    fn name(&self) -> &'static str {
        "dot_product"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() != 2 {
            Err(InvalidInputDimsError::new(
                "Dot product requires exactly 2 inputs.",
                input_dims,
                self.name(),
            ))
        } else if input_dims[0].1 != input_dims[1].0 {
            Err(InvalidInputDimsError::new(
                "Number of columns in left matrix must match number of rows in right matrix.",
                input_dims,
                self.name(),
            ))
        } else {
            Ok((input_dims[0].0, input_dims[1].1))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        inputs[0].dot(inputs[1])
    }

    fn derivative(
        &self,
        _index: (usize, usize),
        _wrt_child: usize,
        _self_ref: RefDims<comp_net::OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(comp_net::CompNetBuilder, Vec<NodeRefAny>)> {
        None
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        self_ref: RefDims<comp_net::OperatorRef>,
        prev_derivative: RefDims<comp_net::OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert!(wrt_child < 2);
        assert_eq!(inputs.len(), 2);
        assert_eq!(inputs[0].dims.1, inputs[1].dims.0);
        assert_eq!(self_ref.dims.0, inputs[0].dims.0);
        assert_eq!(self_ref.dims.1, inputs[1].dims.1);

        Some(match wrt_child {
            0 => {
                let mut result = CompNetBuilder::new();
                let prev_derivative_source =
                    result.add_source(Matrix::from_elem(prev_derivative.dims, 0.));
                let rhs_source = result.add_source(Matrix::from_elem(inputs[1].dims, 0.));
                let transpose_ref = result.add_operator(OperatorTranspose, vec![rhs_source]);
                let _derivative_ref = result.add_operator(
                    OperatorDotProduct,
                    vec![prev_derivative_source.as_any(), transpose_ref.as_any()],
                );
                (
                    result,
                    vec![
                        prev_derivative.node_ref.as_any(),
                        inputs[1].node_ref.as_any(),
                    ],
                )
            }
            1 => {
                let mut result = CompNetBuilder::new();
                let lhs_source = result.add_source(Matrix::from_elem(inputs[0].dims, 0.));
                let prev_derivative_source =
                    result.add_source(Matrix::from_elem(prev_derivative.dims, 0.));
                let transpose_ref = result.add_operator(OperatorTranspose, vec![lhs_source]);
                let _derivative_ref = result.add_operator(
                    OperatorDotProduct,
                    vec![transpose_ref.as_any(), prev_derivative_source.as_any()],
                );
                (
                    result,
                    vec![
                        inputs[0].node_ref.as_any(),
                        prev_derivative.node_ref.as_any(),
                    ],
                )
            }
            _ => {
                unreachable!();
            }
        })
    }
}

#[cfg(test)]
mod test {
    use crate::comp_net::OperatorIdentity;

    use super::*;

    #[test]
    fn derivative_test() {
        let mut builder = CompNetBuilder::new();
        let a = builder.add_source_with_name(
            Matrix::from_shape_vec((2, 2), vec![1., 2., 3., 4.]).unwrap(),
            "a",
        );
        let b = builder.add_source_with_name(
            Matrix::from_shape_vec((2, 2), vec![5., 6., 7., 8.]).unwrap(),
            "b",
        );
        let f =
            builder.add_operator_with_name(OperatorDotProduct, vec![a.as_any(), b.as_any()], "f");
        let g = builder.add_operator_with_name(OperatorIdentity, vec![f.as_any()], "g");
        let dzda_0_0 = builder.differentiate_single_with_name(g, (0, 0), a, "dzda");
        let dzda_0_1 = builder.differentiate_single_with_name(g, (0, 1), a, "dzda");
        let dzda_1_0 = builder.differentiate_single_with_name(g, (1, 0), a, "dzda");
        let dzda_1_1 = builder.differentiate_single_with_name(g, (1, 1), a, "dzda");

        let mut comp_net = builder.build();
        comp_net.compute(dzda_0_0);
        comp_net.compute(dzda_0_1);
        comp_net.compute(dzda_1_0);
        comp_net.compute(dzda_1_1);
        let dzda_0_0_value = comp_net.node_value(dzda_0_0).unwrap();
        let dzda_0_1_value = comp_net.node_value(dzda_0_1).unwrap();
        let dzda_1_0_value = comp_net.node_value(dzda_1_0).unwrap();
        let dzda_1_1_value = comp_net.node_value(dzda_1_1).unwrap();

        assert_eq!(
            dzda_0_0_value,
            Matrix::from_shape_vec((2, 2), vec![5., 7., 0., 0.]).unwrap()
        );
        assert_eq!(
            dzda_0_1_value,
            Matrix::from_shape_vec((2, 2), vec![6., 8., 0., 0.]).unwrap()
        );
        assert_eq!(
            dzda_1_0_value,
            Matrix::from_shape_vec((2, 2), vec![0., 0., 5., 7.]).unwrap()
        );
        assert_eq!(
            dzda_1_1_value,
            Matrix::from_shape_vec((2, 2), vec![0., 0., 6., 8.]).unwrap()
        );
    }
}
