use crate::{
    comp_net::{
        CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorConstant,
        OperatorElementSum, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorMatrixFromValue {
    dims: (usize, usize),
}

impl OperatorMatrixFromValue {
    pub fn new(dims: (usize, usize)) -> Self {
        Self { dims }
    }
}

impl CompNetNodeFuncs for OperatorMatrixFromValue {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self { dims: self.dims })
    }

    fn name(&self) -> &'static str {
        "matrix_from_value"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() == 1 {
            if input_dims[0] == (1, 1) {
                Ok(self.dims)
            } else {
                Err(InvalidInputDimsError::new(
                    "Input must be a scalar.",
                    input_dims,
                    self.name(),
                ))
            }
        } else {
            Err(InvalidInputDimsError::new(
                "Takes exactly one input.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 1);
        assert_eq!(inputs[0].dim(), (1, 1));
        Matrix::from_elem(self.dims, inputs[0][(0, 0)])
    }

    fn derivative(
        &self,
        index: (usize, usize),
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(self_ref.dims, self.dims);
        assert!(index.0 < self_ref.dims.0);
        assert!(index.1 < self_ref.dims.1);
        assert_eq!(inputs.len(), 1);
        assert!(wrt_child < inputs.len());
        let value = Matrix::ones((1, 1));
        Some(super::funcs_to_subnet(OperatorConstant::new(value), vec![]))
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<crate::comp_net::NodeRefAny>)> {
        assert_eq!(self_ref.dims, self.dims);
        assert_eq!(inputs.len(), 1);
        assert_eq!(inputs[0].dims, (1, 1));
        assert!(wrt_child < inputs.len());
        Some(super::funcs_to_subnet(
            OperatorElementSum,
            vec![prev_derivative.as_any()],
        ))
    }
}
