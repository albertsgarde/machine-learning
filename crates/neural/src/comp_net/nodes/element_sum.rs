use crate::{
    comp_net::{
        CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, NodeRefAny, OperatorConstant,
        OperatorMatrixFromValue, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorElementSum;

impl CompNetNodeFuncs for OperatorElementSum {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self)
    }

    fn name(&self) -> &'static str {
        "element_sum"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() == 1 {
            Ok((1, 1))
        } else {
            Err(InvalidInputDimsError::new(
                "Takes exactly one input.",
                input_dims,
                self.name(),
            ))
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 1);
        let value = inputs[0].sum();
        Matrix::from_diag_elem(1, value)
    }

    fn derivative(
        &self,
        index: (usize, usize),
        wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(index, (0, 0));
        assert_eq!(inputs.len(), 1);
        assert!(wrt_child < inputs.len());
        let mut value = Matrix::ones(inputs[0].dims);
        value[index] = 1.0;
        Some(super::funcs_to_subnet(OperatorConstant::new(value), vec![]))
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(inputs.len(), 1);
        assert!(wrt_child < inputs.len());
        assert_eq!(self_ref.dims, (1, 1));

        Some(super::funcs_to_subnet(
            OperatorMatrixFromValue::new(inputs[0].dims),
            vec![prev_derivative.as_any()],
        ))
    }
}
