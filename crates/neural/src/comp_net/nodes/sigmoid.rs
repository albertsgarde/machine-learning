use crate::{
    comp_net::{
        CompNetBuilder, CompNetNodeFuncs, NodeRef, NodeRefAny, OperatorAdd, OperatorConstant,
        OperatorMultiply, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorSigmoid;

impl CompNetNodeFuncs for OperatorSigmoid {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorSigmoid)
    }

    fn name(&self) -> &'static str {
        "sigmoid"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), crate::comp_net::InvalidInputDimsError> {
        if input_dims.len() != 1 {
            Err(crate::comp_net::InvalidInputDimsError::new(
                "Sigmoid takes exactly 1 input.",
                input_dims,
                self.name(),
            ))
        } else {
            Ok(input_dims[0])
        }
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        assert_eq!(inputs.len(), 1, "Sigmoid takes exactly 1 input.");
        let input = inputs[0];
        input.map(|x| 1.0 / (1.0 + (-x).exp()))
    }

    fn chain_derivative(
        &self,
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        prev_derivative: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(wrt_child, 0, "Sigmoid only has one input.");
        assert_eq!(inputs.len(), 1, "Sigmoid takes exactly one input.");
        let mut result = CompNetBuilder::new();
        let matrix_of_ones = result.add_operator::<_, OperatorRef, _>(
            OperatorConstant::new(Matrix::from_elem(self_ref.dims, 1.)),
            &[],
        );
        let matrix_of_negative_ones = result.add_operator::<_, OperatorRef, _>(
            OperatorConstant::new(Matrix::from_elem(self_ref.dims, -1.)),
            &[],
        );
        let self_source = result.add_source(Matrix::from_elem(self_ref.dims, 0.));

        let negate_ref = result.add_operator(
            OperatorMultiply,
            &[matrix_of_negative_ones.as_any(), self_source.as_any()],
        );
        let one_minus_self =
            result.add_operator(OperatorAdd, &[matrix_of_ones.as_any(), negate_ref.as_any()]);
        let sigmoid_derivative = result.add_operator(
            OperatorMultiply,
            &[self_source.as_any(), one_minus_self.as_any()],
        );
        let _derivative = result.add_operator(
            OperatorMultiply,
            &[
                sigmoid_derivative.as_any(),
                prev_derivative.node_ref.as_any(),
            ],
        );
        Some((
            result,
            vec![self_ref.node_ref.as_any(), inputs[1].node_ref.as_any()],
        ))
    }
}
