use crate::{
    comp_net::{
        nodes::funcs_to_subnet, CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError,
        NodeRefAny, OperatorLogLossDerivative, OperatorRef, RefDims,
    },
    Matrix,
};

pub struct OperatorLogLoss;
impl CompNetNodeFuncs for OperatorLogLoss {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(OperatorLogLoss)
    }

    fn name(&self) -> &'static str {
        "log_loss"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        if input_dims.len() != 2 {
            Err(InvalidInputDimsError::new(
                "Log loss operator takes exactly two inputs.",
                input_dims,
                self.name(),
            ))
        } else if input_dims[0] != input_dims[1] {
            Err(InvalidInputDimsError::new(
                "Log loss operator takes inputs of the same size.",
                input_dims,
                self.name(),
            ))
        } else {
            Ok((1, 1))
        }
    }

    fn compute(&self, inputs: &[&Matrix]) -> Matrix {
        assert_eq!(inputs.len(), 2);
        assert_eq!(inputs[0].dim(), inputs[1].dim());
        let y = inputs[0];
        let estimated_y = inputs[1];
        let value = y
            .iter()
            .zip(estimated_y.iter())
            .map(|(y, estimated_y)| -y * estimated_y.ln() - (1. - y) * (1. - estimated_y).ln())
            .sum();
        Matrix::from_elem((1, 1), value)
    }

    fn derivative(
        &self,
        index: (usize, usize),
        wrt_child: usize,
        self_ref: RefDims<OperatorRef>,
        inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        assert_eq!(index, (0, 0));
        assert_eq!(self_ref.dims, (0, 0));
        assert_eq!(inputs.len(), 2);
        assert_eq!(inputs[0].dims, inputs[1].dims);
        assert!(wrt_child < 2);
        match wrt_child {
            0 => None,
            1 => Some(funcs_to_subnet(OperatorLogLossDerivative, inputs)),
            _ => unreachable!(),
        }
    }
}
