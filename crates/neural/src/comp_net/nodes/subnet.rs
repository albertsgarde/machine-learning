use crate::comp_net::{CompNetBuilder, CompNetNodeFuncs, InvalidInputDimsError, OperatorRef};

#[derive(Clone)]
pub struct OperatorSubnet {
    subnet: CompNetBuilder,
    output: OperatorRef,
}

impl OperatorSubnet {
    pub fn new(subnet: CompNetBuilder, output: OperatorRef) -> Self {
        Self { subnet, output }
    }
}

impl CompNetNodeFuncs for OperatorSubnet {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs> {
        Box::new(Self {
            subnet: self.subnet.clone(),
            output: self.output,
        })
    }

    fn name(&self) -> &'static str {
        "subnet"
    }

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError> {
        let (_, children) = self.subnet.clone().build_and_children();
        if input_dims.len() != children.len() {
            return Err(InvalidInputDimsError::new(
                "Number of inputs must match the number of sources in the subnet.",
                input_dims,
                self.name(),
            ));
        }
        for (index, (&dims, &child_ref)) in input_dims.iter().zip(children.iter()).enumerate() {
            if self.subnet.node_dims(child_ref) != dims {
                return Err(InvalidInputDimsError::new(
                    format!("Input dimensions must match the dimensions of the sources in the subnet. Index: {index}  Input dims: {dims:?}  Source ref: {child_ref:?}  Source dims: {:?}", self.subnet.node_dims(child_ref)),
                    input_dims,
                    self.name(),
                ));
            }
        }

        Ok(self.subnet.node_dims(self.output))
    }

    fn compute(&self, inputs: &[&crate::Matrix]) -> crate::Matrix {
        let (mut comp_net, children) = self.subnet.clone().build_and_children();
        assert_eq!(inputs.len(), children.len());
        assert!(inputs.windows(2).all(|w| w[0].dim() == w[1].dim()));

        for (&input, &child_ref) in inputs.iter().zip(children.iter()) {
            comp_net.set_value(child_ref, input.clone());
        }
        comp_net.compute(self.output).clone()
    }
}
