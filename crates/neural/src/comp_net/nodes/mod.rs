mod add;
pub use add::OperatorAdd;
mod constant;
pub use constant::OperatorConstant;
mod identity;
pub use identity::OperatorIdentity;
mod element_sum;
pub use element_sum::OperatorElementSum;
mod matrix_from_value;
pub use matrix_from_value::OperatorMatrixFromValue;
mod multiply;
pub use multiply::OperatorMultiply;
mod subnet;
pub use subnet::OperatorSubnet;
mod dot_product;
pub use dot_product::OperatorDotProduct;
mod transpose;
pub use transpose::OperatorTranspose;
mod sigmoid;
pub use sigmoid::OperatorSigmoid;
mod log_loss;
pub use log_loss::OperatorLogLoss;
mod log_loss_derivative;
pub use log_loss_derivative::OperatorLogLossDerivative;

use crate::Matrix;

use super::{CompNetBuilder, CompNetNodeFuncs, NodeRefAny, RefDims};

fn funcs_to_subnet<F, A>(funcs: F, children: A) -> (CompNetBuilder, Vec<NodeRefAny>)
where
    F: CompNetNodeFuncs,
    A: AsRef<[RefDims<NodeRefAny>]>,
{
    let children = children.as_ref();

    assert!(children.iter().all(
        |&RefDims {
             node_ref: _,
             dims: (rows, cols),
         }| rows > 0 && cols > 0
    ));

    let mut result = CompNetBuilder::new();
    let source_refs: Vec<_> = children
        .iter()
        .map(|&RefDims { node_ref: _, dims }| result.add_source(Matrix::from_elem(dims, 0.)))
        .collect();
    result.add_operator(funcs, source_refs);
    (
        result,
        children
            .iter()
            .map(|&RefDims { node_ref, .. }| node_ref)
            .collect(),
    )
}
