use std::fmt::{Debug, Display};

use crate::Matrix;

use super::{CompNetBuilder, NodeRef, NodeRefAny, OperatorRef};

#[derive(Clone, Debug)]
pub struct DerivativeInfo {
    target: OperatorRef,
    target_index: (usize, usize),
    wrt: NodeRefAny,
}

impl DerivativeInfo {
    pub fn new(target: OperatorRef, target_index: (usize, usize), wrt: NodeRefAny) -> Self {
        Self {
            target,
            target_index,
            wrt,
        }
    }

    pub fn chain(&self, wrt: NodeRefAny) -> Self {
        Self {
            target: self.target,
            target_index: self.target_index,
            wrt,
        }
    }

    pub fn target(&self) -> OperatorRef {
        self.target
    }

    pub fn target_index(&self) -> (usize, usize) {
        self.target_index
    }

    pub fn wrt(&self) -> NodeRefAny {
        self.wrt
    }
}

#[derive(Clone, Debug)]
pub struct Source {
    dims: (usize, usize),
    value: Matrix,
    dependents: Vec<OperatorRef>,
    name: Option<String>,
}

pub struct Operator {
    dims: (usize, usize),
    value: Option<Matrix>,
    derivative_info: Option<DerivativeInfo>,
    funcs: Box<dyn CompNetNodeFuncs>,
    dependents: Vec<OperatorRef>,
    name: Option<String>,
}

impl Source {
    pub fn new(value: Matrix, name: Option<String>) -> Self {
        Self {
            dims: value.dim(),
            value,
            dependents: vec![],
            name,
        }
    }

    pub fn dims(&self) -> (usize, usize) {
        self.dims
    }

    pub fn value(&self) -> &Matrix {
        &self.value
    }

    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    pub fn set_value(&mut self, value: Matrix) {
        assert_eq!(self.dims, value.dim());
        self.value = value;
    }

    pub fn dependents(&self) -> &[OperatorRef] {
        &self.dependents
    }

    pub fn add_dependent(&mut self, operator_ref: OperatorRef) {
        self.dependents.push(operator_ref);
    }
}

impl Operator {
    pub(crate) fn new(
        input_dims: &[(usize, usize)],
        funcs: Box<dyn CompNetNodeFuncs>,
        derivative_info: Option<DerivativeInfo>,
        name: Option<String>,
    ) -> Self {
        let dims = funcs
            .dim_from_input_dims(input_dims)
            .expect("Invalid inputs.");
        Self {
            dims,
            value: None,
            funcs,
            derivative_info,
            dependents: vec![],
            name,
        }
    }

    pub fn dims(&self) -> (usize, usize) {
        self.dims
    }

    pub fn derivative_info(&self) -> Option<&DerivativeInfo> {
        self.derivative_info.as_ref()
    }

    pub fn funcs(&self) -> &dyn CompNetNodeFuncs {
        self.funcs.as_ref()
    }

    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    pub fn set_name(&mut self, name: String) {
        self.name = Some(name);
    }

    pub fn dependents(&self) -> &[OperatorRef] {
        &self.dependents
    }

    pub fn clear_value(&mut self) {
        self.value = None;
    }

    pub fn add_dependent(&mut self, operator_ref: OperatorRef) {
        self.dependents.push(operator_ref);
    }

    pub fn set_derivative_info(&mut self, derivative_info: Option<DerivativeInfo>) {
        self.derivative_info = derivative_info;
    }

    pub fn compute(&mut self, inputs: &[&Matrix]) -> &Matrix {
        assert!(
            self.value.is_none(),
            "Value already computed! Node: {:?}",
            self.name
        );
        let new_value = self.funcs.compute(inputs);
        assert_eq!(new_value.dim(), self.dims, "Node: {:?}", self.name);

        self.value.insert(new_value)
    }

    pub fn value(&self) -> Option<&Matrix> {
        self.value.as_ref()
    }
}

impl Clone for Operator {
    fn clone(&self) -> Self {
        Self {
            dims: self.dims,
            value: self.value.clone(),
            funcs: self.funcs.clone(),
            derivative_info: self.derivative_info.clone(),
            dependents: self.dependents.clone(),
            name: self.name.clone(),
        }
    }
}

impl Debug for Operator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Operator")
            .field("dims", &self.dims)
            .field("value", &self.value)
            .field("derivative_info", &self.derivative_info)
            .field("funcs", &self.funcs.name())
            .field("dependents", &self.dependents)
            .finish()
    }
}

#[derive(Debug, Clone)]
pub struct InvalidInputDimsError {
    message: String,
    input_dims: Vec<(usize, usize)>,
    node_type: &'static str,
}

impl InvalidInputDimsError {
    pub fn new<S, A>(message: S, input_dims: A, node_type: &'static str) -> Self
    where
        S: AsRef<str>,
        A: AsRef<[(usize, usize)]>,
    {
        Self {
            message: message.as_ref().to_string(),
            input_dims: input_dims.as_ref().to_vec(),
            node_type,
        }
    }
}

impl Display for InvalidInputDimsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "InvalidInputDimsError  Input dimensions: {:?}  Node type: {}  Message: {}",
            self.input_dims.as_slice(),
            self.node_type,
            self.message
        )
    }
}

#[derive(Clone, Copy)]
pub struct RefDims<R: NodeRef> {
    pub(crate) node_ref: R,
    pub(crate) dims: (usize, usize),
}

impl<R: NodeRef> RefDims<R> {
    pub fn as_any(self) -> RefDims<NodeRefAny> {
        RefDims {
            node_ref: self.node_ref.as_any(),
            dims: self.dims,
        }
    }
}

pub trait CompNetNodeFuncs {
    fn clone(&self) -> Box<dyn CompNetNodeFuncs>;

    fn name(&self) -> &'static str;

    fn dim_from_input_dims(
        &self,
        input_dims: &[(usize, usize)],
    ) -> Result<(usize, usize), InvalidInputDimsError>;

    fn compute(&self, inputs: &[&Matrix]) -> Matrix;

    fn derivative(
        &self,
        _index: (usize, usize),
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        None
    }

    fn chain_derivative(
        &self,
        _wrt_child: usize,
        _self_ref: RefDims<OperatorRef>,
        _prev_derivative: RefDims<OperatorRef>,
        _inputs: &[RefDims<NodeRefAny>],
    ) -> Option<(CompNetBuilder, Vec<NodeRefAny>)> {
        None
    }
}
