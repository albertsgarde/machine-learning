use crate::{
    comp_net::{Operator, Source},
    Matrix,
};

use super::{NodeRef, NodeRefAny, NodeType, OperatorRef, SourceRef};

#[derive(Clone)]
pub struct CompNet {
    sources: Vec<Source>,
    operators: Vec<(Operator, Vec<NodeRefAny>)>,
}

impl CompNet {
    pub(super) fn new(sources: Vec<Source>, operators: Vec<(Operator, Vec<NodeRefAny>)>) -> Self {
        Self { sources, operators }
    }

    pub fn node_value<R>(&self, node_ref: R) -> Option<&Matrix>
    where
        R: NodeRef,
    {
        match node_ref.node_type() {
            NodeType::Source => Some(self.sources[node_ref.index()].value()),
            NodeType::Operator => self.operators[node_ref.index()].0.value(),
        }
    }

    pub fn source_value(&self, node_ref: SourceRef) -> &Matrix {
        self.sources[node_ref.index()].value()
    }

    pub fn operator_value(&self, node_ref: OperatorRef) -> Option<&Matrix> {
        self.operators[node_ref.index()].0.value()
    }

    pub fn node_dims<R: NodeRef>(&self, node_ref: R) -> (usize, usize) {
        match node_ref.node_type() {
            NodeType::Source => self.sources[node_ref.index()].dims(),
            NodeType::Operator => self.operators[node_ref.index()].0.dims(),
        }
    }

    pub fn node_name<R: NodeRef>(&self, node_ref: R) -> Option<&str> {
        match node_ref.node_type() {
            NodeType::Source => self.sources[node_ref.index()].name(),
            NodeType::Operator => self.operators[node_ref.index()].0.name(),
        }
    }

    fn clear_value_int(operators: &mut [(Operator, Vec<NodeRefAny>)], offset: usize, index: usize) {
        assert!(!operators.is_empty());
        assert!(index < operators.len());
        let (lower_subtree, upper_subtree) = operators.split_at_mut(index + 1);
        let new_offset = offset + lower_subtree.len();
        let (node, _) = lower_subtree.last_mut().unwrap();
        node.clear_value();
        for &dependent in node.dependents() {
            Self::clear_value_int(upper_subtree, new_offset, dependent.index() - new_offset);
        }
    }

    pub fn clear_value(&mut self, node_ref: OperatorRef) {
        Self::clear_value_int(self.operators.as_mut_slice(), 0, node_ref.index());
    }

    pub fn set_value(&mut self, node_ref: SourceRef, value: Matrix) {
        let source = &self.sources[node_ref.index()];
        assert_eq!(value.dim(), source.dims());
        for &dependent in self.sources[node_ref.index()].dependents() {
            Self::clear_value_int(self.operators.as_mut_slice(), 0, dependent.index());
        }
        self.sources[node_ref.index()].set_value(value);
    }

    pub fn compute(&mut self, node_ref: OperatorRef) -> &Matrix {
        let mut check_nodes = vec![node_ref];
        // Find which other operator nodes in the net are required.
        let mut required_nodes = vec![];
        while let Some(node_ref) = check_nodes.pop() {
            let (node, children) = &self.operators[node_ref.index()];
            if node.value().is_none() {
                required_nodes.push(node_ref);
                for &child in children {
                    if let NodeRefAny::Operator(child_ref) = child {
                        check_nodes.push(child_ref);
                    }
                }
            }
        }
        // Sort required nodes topologically.
        required_nodes.sort_unstable_by_key(|node_ref| node_ref.index());
        required_nodes.dedup();
        for node_ref in required_nodes {
            let (lower_subtree, upper_subtree) = self.operators.split_at_mut(node_ref.index());
            let ((node, children), _) = upper_subtree.split_first_mut().unwrap();

            let inputs = children
                .iter()
                .map(|&child| match child {
                    NodeRefAny::Source(source_ref) => self.sources[source_ref.index()].value(),
                    NodeRefAny::Operator(operator_ref) => lower_subtree[operator_ref.index()]
                        .0
                        .value()
                        .expect("Children should have been computed."),
                })
                .collect::<Vec<_>>();
            node.compute(&inputs);
        }

        self.operators[node_ref.index()]
            .0
            .value()
            .expect("Node should have been computed.")
    }
}
