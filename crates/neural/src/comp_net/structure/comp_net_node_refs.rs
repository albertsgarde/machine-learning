#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SourceRef {
    pub(super) index: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OperatorRef {
    pub(super) index: usize,
}

mod sealed {
    pub trait Sealed {}
}

impl sealed::Sealed for SourceRef {}
impl sealed::Sealed for OperatorRef {}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum NodeType {
    Source,
    Operator,
}

pub trait NodeRef: sealed::Sealed + Clone + Copy {
    fn index(self) -> usize;
    fn node_type(self) -> NodeType;
    fn as_any(self) -> NodeRefAny;
    fn as_source(self) -> Option<SourceRef>;
    fn as_operator(self) -> Option<OperatorRef>;
}

impl NodeRef for SourceRef {
    fn index(self) -> usize {
        self.index
    }

    fn node_type(self) -> NodeType {
        NodeType::Source
    }
    fn as_any(self) -> NodeRefAny {
        NodeRefAny::Source(self)
    }

    fn as_source(self) -> Option<SourceRef> {
        Some(self)
    }

    fn as_operator(self) -> Option<OperatorRef> {
        None
    }
}

impl NodeRef for OperatorRef {
    fn index(self) -> usize {
        self.index
    }

    fn node_type(self) -> NodeType {
        NodeType::Operator
    }
    fn as_any(self) -> NodeRefAny {
        NodeRefAny::Operator(self)
    }

    fn as_source(self) -> Option<SourceRef> {
        None
    }

    fn as_operator(self) -> Option<OperatorRef> {
        Some(self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum NodeRefAny {
    Source(SourceRef),
    Operator(OperatorRef),
}

impl sealed::Sealed for NodeRefAny {}

impl NodeRef for NodeRefAny {
    fn index(self) -> usize {
        match self {
            NodeRefAny::Source(SourceRef { index }) => index,
            NodeRefAny::Operator(OperatorRef { index }) => index,
        }
    }

    fn node_type(self) -> NodeType {
        match self {
            NodeRefAny::Source(_) => NodeType::Source,
            NodeRefAny::Operator(_) => NodeType::Operator,
        }
    }

    fn as_any(self) -> NodeRefAny {
        self
    }

    fn as_source(self) -> Option<SourceRef> {
        match self {
            NodeRefAny::Source(source) => Some(source),
            _ => None,
        }
    }

    fn as_operator(self) -> Option<OperatorRef> {
        match self {
            NodeRefAny::Operator(operator) => Some(operator),
            _ => None,
        }
    }
}
