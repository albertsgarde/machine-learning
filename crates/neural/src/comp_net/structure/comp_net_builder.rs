use json::JsonValue;

use crate::{
    comp_net::{DerivativeInfo, OperatorAdd},
    Matrix,
};

use super::{
    CompNet, CompNetNodeFuncs, NodeRef, NodeRefAny, NodeType, Operator, OperatorRef, RefDims,
    Source, SourceRef,
};

#[derive(Clone, Debug)]
pub struct CompNetBuilder {
    sources: Vec<Source>,
    operators: Vec<(Operator, Vec<NodeRefAny>)>,
}

impl CompNetBuilder {
    pub fn new() -> CompNetBuilder {
        Self {
            sources: vec![],
            operators: vec![],
        }
    }

    pub fn node_name<R: NodeRef>(&self, node_ref: R) -> Option<&str> {
        match node_ref.node_type() {
            NodeType::Source => self.sources[node_ref.index()].name(),
            NodeType::Operator => self.operators[node_ref.index()].0.name(),
        }
    }

    fn ref_string_rep<R: NodeRef>(node_ref: R) -> String {
        match node_ref.node_type() {
            NodeType::Source => format!("S{}", node_ref.index()),
            NodeType::Operator => format!("O{}", node_ref.index()),
        }
    }

    pub fn node_name_or_index<R: NodeRef>(&self, node_ref: R) -> String {
        self.node_name(node_ref)
            .map(|name| name.to_string())
            .unwrap_or_else(|| Self::ref_string_rep(node_ref))
    }

    fn add_dependent<R: NodeRef>(&mut self, node_ref: R, dependent: OperatorRef) {
        match node_ref.node_type() {
            NodeType::Source => self.sources[node_ref.index()].add_dependent(dependent),
            NodeType::Operator => self.operators[node_ref.index()].0.add_dependent(dependent),
        }
    }

    fn child_dims<R: NodeRef>(&self, child: R) -> (usize, usize) {
        match child.node_type() {
            NodeType::Source => self.sources[child.index()].dims(),
            NodeType::Operator => self.operators[child.index()].0.dims(),
        }
    }

    pub fn node_dims<R: NodeRef>(&self, node: R) -> (usize, usize) {
        match node.node_type() {
            NodeType::Source => self.sources[node.index()].dims(),
            NodeType::Operator => self.operators[node.index()].0.dims(),
        }
    }

    pub fn children(&self, operator_ref: OperatorRef) -> &[NodeRefAny] {
        self.operators[operator_ref.index()].1.as_slice()
    }

    fn add_source_int(&mut self, value: Matrix, name: Option<String>) -> SourceRef {
        let source = Source::new(value, name);
        self.sources.push(source);
        SourceRef {
            index: self.sources.len() - 1,
        }
    }

    pub fn add_source(&mut self, value: Matrix) -> SourceRef {
        self.add_source_int(value, None)
    }

    pub fn add_source_with_name<S: AsRef<str>>(&mut self, value: Matrix, name: S) -> SourceRef {
        self.add_source_int(value, Some(name.as_ref().to_string()))
    }

    fn add_operator_int<R: NodeRef>(
        &mut self,
        operator: Operator,
        children: Vec<R>,
    ) -> OperatorRef {
        for &child in children.iter() {
            match child.node_type() {
                NodeType::Source => assert!(child.index() < self.sources.len()),
                NodeType::Operator => {
                    assert!(child.index() < self.operators.len())
                }
            }
        }
        let operator_ref = OperatorRef {
            index: self.operators.len(),
        };
        for &child in children.iter() {
            self.add_dependent(child, operator_ref);
        }
        self.operators.push((
            operator,
            children
                .into_iter()
                .map(|child_ref| child_ref.as_any())
                .collect(),
        ));
        operator_ref
    }

    pub fn add_operator<F, R, A>(&mut self, funcs: F, children: A) -> OperatorRef
    where
        F: CompNetNodeFuncs,
        R: NodeRef,
        A: AsRef<[R]>,
    {
        let input_dims: Vec<_> = children
            .as_ref()
            .iter()
            .map(|&child| self.child_dims(child))
            .collect();
        let operator = Operator::new(&input_dims, funcs.clone(), None, None);

        self.add_operator_int(operator, children.as_ref().to_vec())
    }

    pub fn add_operator_with_name<F, R, A, S>(
        &mut self,
        funcs: F,
        children: A,
        name: S,
    ) -> OperatorRef
    where
        F: CompNetNodeFuncs,
        R: NodeRef,
        A: AsRef<[R]>,
        S: AsRef<str>,
    {
        let input_dims: Vec<_> = children
            .as_ref()
            .iter()
            .map(|&child| self.child_dims(child))
            .collect();
        let operator = Operator::new(
            &input_dims,
            funcs.clone(),
            None,
            Some(name.as_ref().to_string()),
        );

        self.add_operator_int(operator, children.as_ref().to_vec())
    }

    fn add_derivative_int(
        &self,
        operator_ref: OperatorRef,
        index: (usize, usize),
        wrt_child: usize,
        name: Option<String>,
    ) -> (CompNetBuilder, Vec<NodeRefAny>) {
        let (operator, children) = &self.operators[operator_ref.index()];
        assert!(index.0 < operator.dims().0);
        assert!(index.1 < operator.dims().1);
        assert!(wrt_child < children.len());
        let operator_inputs = children
            .iter()
            .map(|&child| RefDims {
                node_ref: child,
                dims: self.child_dims(child),
            })
            .collect::<Vec<_>>();
        if let Some((mut derivative_comp_net, derivative_inputs)) = operator.funcs().derivative(
            index,
            wrt_child,
            RefDims {
                node_ref: operator_ref,
                dims: operator.dims(),
            },
            operator_inputs.as_slice(),
        ) {
            let derivative_info = Some(DerivativeInfo::new(
                operator_ref,
                index,
                children[wrt_child],
            ));

            derivative_comp_net
                .operators
                .last_mut()
                .expect("Derivative comp nets must have at least one operator.")
                .0
                .set_derivative_info(derivative_info);
            if let Some(name) = name {
                derivative_comp_net
                    .operators
                    .last_mut()
                    .expect("Derivative comp nets must have at least one operator.")
                    .0
                    .set_name(name);
            }

            (derivative_comp_net, derivative_inputs)
        } else {
            panic!(
                "Derivative not implemented for node of type '{}'.",
                operator.funcs().name()
            );
        }
    }

    pub fn add_derivative(
        &mut self,
        operator_ref: OperatorRef,
        index: (usize, usize),
        wrt_child: usize,
    ) -> OperatorRef {
        let (derivative_comp_net, derivative_inputs) =
            self.add_derivative_int(operator_ref, index, wrt_child, None);
        *self
            .add_comp_net(
                derivative_comp_net,
                derivative_inputs.into_iter().map(Some).collect::<Vec<_>>(),
            )
            .1
            .last()
            .expect("Internal error. Should have been captured in add_derivative_int.")
    }

    pub fn add_derivative_with_name<S: AsRef<str>>(
        &mut self,
        operator_ref: OperatorRef,
        index: (usize, usize),
        wrt_child: usize,
        name: S,
    ) -> OperatorRef {
        let (derivative_comp_net, derivative_inputs) = self.add_derivative_int(
            operator_ref,
            index,
            wrt_child,
            Some(name.as_ref().to_string()),
        );
        *self
            .add_comp_net(
                derivative_comp_net,
                derivative_inputs.into_iter().map(Some).collect::<Vec<_>>(),
            )
            .1
            .last()
            .expect("Internal error. Should have been captured in add_derivative_int.")
    }

    fn chain_derivative_int(
        &self,
        derivative_ref: OperatorRef,
        wrt_child: usize,
        name: Option<String>,
    ) -> (CompNetBuilder, Vec<NodeRefAny>) {
        let (prev_derivative, _) = &self.operators[derivative_ref.index()];
        let prev_derivative_info = prev_derivative.derivative_info().unwrap();
        let chain_target_ref = prev_derivative_info.wrt();
        let chain_target_ref = match chain_target_ref {
            NodeRefAny::Source(_) => panic!("Cannot chain derivative from source."),
            NodeRefAny::Operator(operator_ref) => operator_ref,
        };
        let (chain_target, chain_target_inputs) = &self.operators[chain_target_ref.index()];
        let chain_target_inputs = chain_target_inputs
            .iter()
            .map(|&child| RefDims {
                node_ref: child,
                dims: self.child_dims(child),
            })
            .collect::<Vec<_>>();
        if let Some((mut derivative_comp_net, derivative_inputs)) =
            chain_target.funcs().chain_derivative(
                wrt_child,
                RefDims {
                    node_ref: chain_target_ref,
                    dims: chain_target.dims(),
                },
                RefDims {
                    node_ref: derivative_ref,
                    dims: self.node_dims(derivative_ref),
                },
                chain_target_inputs.as_slice(),
            )
        {
            let derivative_info =
                Some(prev_derivative_info.chain(chain_target_inputs[wrt_child].node_ref));
            derivative_comp_net
                .operators
                .last_mut()
                .expect("Derivative comp nets must have at least one operator.")
                .0
                .set_derivative_info(derivative_info);
            if let Some(name) = name {
                derivative_comp_net
                    .operators
                    .last_mut()
                    .expect("Derivative comp nets must have at least one operator.")
                    .0
                    .set_name(name);
            }

            (derivative_comp_net, derivative_inputs)
        } else {
            panic!(
                "Chain derivative not implemented for node of type '{}'.",
                chain_target.funcs().name()
            );
        }
    }

    pub fn chain_derivative(
        &mut self,
        derivative_ref: OperatorRef,
        wrt_child: usize,
    ) -> OperatorRef {
        let (derivative_comp_net, derivative_inputs) =
            self.chain_derivative_int(derivative_ref, wrt_child, None);
        *self
            .add_comp_net(
                derivative_comp_net,
                derivative_inputs.into_iter().map(Some).collect::<Vec<_>>(),
            )
            .1
            .last()
            .expect("Internal error. Should have been captured in add_derivative_int.")
    }

    pub fn chain_derivative_with_name<S: AsRef<str>>(
        &mut self,
        derivative_ref: OperatorRef,
        wrt_child: usize,
        name: S,
    ) -> OperatorRef {
        let (derivative_comp_net, derivative_inputs) =
            self.chain_derivative_int(derivative_ref, wrt_child, Some(name.as_ref().to_string()));
        *self
            .add_comp_net(
                derivative_comp_net,
                derivative_inputs.into_iter().map(Some).collect::<Vec<_>>(),
            )
            .1
            .last()
            .expect("Internal error. Should have been captured in add_derivative_int.")
    }

    fn find_dependents<R: NodeRef>(&self, node_ref: R, dependents: &mut [bool]) {
        assert_eq!(dependents.len(), self.operators.len());

        let direct_dependents = match node_ref.node_type() {
            NodeType::Source => self.sources[node_ref.index()].dependents(),
            NodeType::Operator => self.operators[node_ref.index()].0.dependents(),
        };

        for &dependent in direct_dependents {
            if !dependents[dependent.index()] {
                dependents[dependent.index()] = true;
                self.find_dependents(dependent, dependents);
            }
        }
    }

    fn find_descendants(&self, node_ref: OperatorRef, descendants: &mut [bool]) {
        assert_eq!(descendants.len(), self.operators.len());

        let direct_descendants = self.operators[node_ref.index()].1.as_slice();
        for &descendant in direct_descendants {
            if let Some(descendant) = descendant.as_operator() {
                if !descendants[descendant.index()] {
                    descendants[descendant.index()] = true;
                    self.find_descendants(descendant, descendants);
                }
            }
        }
    }

    fn derive_wrt_target_children<D>(
        &mut self,
        node_ref: OperatorRef,
        targets: &[bool],
        prev_derivative: Option<OperatorRef>,
        dependent_derivatives: &mut [Vec<OperatorRef>],
        derivative_function: D,
    ) where
        D: Fn(&Self, Option<OperatorRef>, usize) -> (CompNetBuilder, Vec<NodeRefAny>),
    {
        let children = self.operators[dbg!(node_ref.index())].1.clone();
        dbg!(&children);
        for (child_index, &child_ref) in children
            .iter()
            .enumerate()
            .filter(|(_, &child)| child.node_type() == NodeType::Operator && targets[child.index()])
        {
            let (derivative, derivative_inputs) =
                derivative_function(self, prev_derivative, child_index);
            let derivative_ref = *self
                .add_comp_net(
                    derivative,
                    derivative_inputs.into_iter().map(Some).collect::<Vec<_>>(),
                )
                .1
                .last()
                .unwrap();
            dependent_derivatives[child_ref.index()].push(derivative_ref);
        }
    }

    /// Returns a vector of bools which is true when the operator at the given index is both a descendant of the sink and dependant on the source.
    fn subgraph_members<A>(&self, sources: A, sink: OperatorRef) -> Vec<bool>
    where
        A: IntoIterator<Item = NodeRefAny>,
    {
        let source_dependents = {
            let mut result = vec![false; self.operators.len()];
            for source in sources {
                self.find_dependents(source, &mut result);
            }
            result
        };

        let sink_descendants = {
            let mut result = vec![false; self.operators.len()];
            self.find_descendants(sink, &mut result);
            result
        };

        source_dependents
            .into_iter()
            .zip(sink_descendants.into_iter())
            .map(|(source_dependent, sink_descendant)| source_dependent && sink_descendant)
            .collect()
    }

    fn differentiate_int<A, AS>(
        &mut self,
        target: OperatorRef,
        index: (usize, usize),
        wrts: A,
        wrt_names: AS,
    ) -> Vec<OperatorRef>
    where
        A: AsRef<[NodeRefAny]>,
        AS: AsRef<[Option<String>]>,
    {
        let wrts = wrts.as_ref();
        assert_eq!(wrts.len(), wrt_names.as_ref().len());

        // The direct children of the target.
        let target_children = {
            let (operator, children) = &self.operators[target.index()];
            assert!(index.0 < operator.dims().0);
            assert!(index.1 < operator.dims().1);
            children.clone()
        };

        // All nodes that need to be differentiated before the wrts can be differentiated.
        let targets = self.subgraph_members(wrts.iter().copied(), target);

        // The partial derivatives of the target with respect to each of the above targets.
        let mut dependent_derivatives: Vec<Vec<OperatorRef>> = vec![vec![]; targets.len()];
        // The partial derivatives of the target with respect to each of the wrts.
        let mut wrt_dependent_derivatives: Vec<Vec<OperatorRef>> = vec![vec![]; wrts.len()];

        self.derive_wrt_target_children(
            target,
            targets.as_slice(),
            None,
            &mut dependent_derivatives,
            |this, prev_derivative, child_index| {
                assert!(prev_derivative.is_none());
                this.add_derivative_int(target, index, child_index, None)
            },
        );
        // If wrt is a direct child of target, add that derivative directly.
        for (&wrt, wrt_dependent_derivatives) in
            wrts.iter().zip(wrt_dependent_derivatives.iter_mut())
        {
            if let Some(wrt_index) = target_children.iter().position(|&child| child == wrt) {
                let derivative = self.add_derivative(target, index, wrt_index);
                wrt_dependent_derivatives.push(derivative);
            }
        }

        // Go through all nodes in subgraph and do the same as for the target, except now chaining the derivative instead of starting a new one.
        for (operator_index, _) in dbg!(&targets)
            .iter()
            .enumerate()
            .rev()
            .filter(|(_, &is_target)| is_target)
        {
            // Collect partial derivatives into sum
            assert!(
                !dependent_derivatives[operator_index].is_empty(),
                "No derivatives for operator {}",
                operator_index
            );
            let dependent_derivative_sum_ref = if dependent_derivatives[operator_index].len() > 1 {
                self.add_operator(OperatorAdd, dependent_derivatives[operator_index].clone())
            } else {
                dependent_derivatives[operator_index][0]
            };

            // Find derivatives of children.
            let (_, children) = &self.operators[operator_index];
            let children = children.clone();
            self.derive_wrt_target_children(
                OperatorRef {
                    index: operator_index,
                },
                targets.as_slice(),
                Some(dependent_derivative_sum_ref),
                &mut dependent_derivatives,
                |this, prev_derivative, child_index| {
                    this.chain_derivative_int(prev_derivative.unwrap(), child_index, None)
                },
            );

            // If wrt is a direct child of the operator, add that derivative directly.
            for (&wrt, wrt_dependent_derivatives) in
                wrts.iter().zip(wrt_dependent_derivatives.iter_mut())
            {
                if let Some(wrt_index) = children.iter().position(|&child| child == wrt) {
                    let derivative = self.chain_derivative(dependent_derivative_sum_ref, wrt_index);
                    wrt_dependent_derivatives.push(derivative);
                }
            }
        }

        for wrt_dependent_derivatives in wrt_dependent_derivatives.iter() {
            assert!(!wrt_dependent_derivatives.is_empty());
        }
        // Create the resulting derivative nodes.
        wrts.iter()
            .zip(wrt_names.as_ref())
            .zip(wrt_dependent_derivatives.iter())
            .map(|((wrt, name), wrt_dependent_derivatives)| {
                if wrt_dependent_derivatives.len() > 1 {
                    let derivative_operator = if let Some(name) = name {
                        self.add_operator_with_name(OperatorAdd, wrt_dependent_derivatives, name)
                    } else {
                        self.add_operator(OperatorAdd, wrt_dependent_derivatives)
                    };
                    self.operators[derivative_operator.index()]
                        .0
                        .set_derivative_info(Some(DerivativeInfo::new(
                            target,
                            index,
                            wrt.as_any(),
                        )));
                    for operator_ref in wrt_dependent_derivatives {
                        self.operators[operator_ref.index()]
                            .0
                            .set_derivative_info(None);
                    }
                    derivative_operator
                } else {
                    if let Some(name) = name {
                        self.operators[wrt_dependent_derivatives[0].index()]
                            .0
                            .set_name(name.into());
                    }
                    wrt_dependent_derivatives[0]
                }
            })
            .collect()
    }

    pub fn differentiate_single<R>(
        &mut self,
        target: OperatorRef,
        index: (usize, usize),
        wrt: R,
    ) -> OperatorRef
    where
        R: NodeRef,
    {
        self.differentiate_int(target, index, vec![wrt.as_any()], vec![None])[0]
    }

    pub fn differentiate<R, I>(
        &mut self,
        target: OperatorRef,
        index: (usize, usize),
        wrts: I,
    ) -> Vec<OperatorRef>
    where
        R: NodeRef,
        I: IntoIterator<Item = R>,
    {
        let wrts = wrts.into_iter().map(|wrt| wrt.as_any()).collect::<Vec<_>>();
        let wrts_len = wrts.len();
        self.differentiate_int(target, index, wrts, vec![Option::<String>::None; wrts_len])
    }

    pub fn differentiate_single_with_name<R, S>(
        &mut self,
        target: OperatorRef,
        index: (usize, usize),
        wrt: R,
        name: S,
    ) -> OperatorRef
    where
        R: NodeRef,
        S: Into<String>,
    {
        self.differentiate_int(target, index, vec![wrt.as_any()], vec![Some(name.into())])[0]
    }

    pub fn differentiate_with_names<R, S, I>(
        &mut self,
        target: OperatorRef,
        index: (usize, usize),
        wrts: I,
    ) -> Vec<OperatorRef>
    where
        R: NodeRef,
        S: Into<String>,
        I: IntoIterator<Item = (R, Option<S>)>,
    {
        let (wrts, names): (Vec<_>, Vec<_>) = wrts
            .into_iter()
            .map(|(wrt, name)| (wrt.as_any(), name.map(|name| name.into())))
            .unzip();
        self.differentiate_int(target, index, wrts, names)
    }

    /// Adds the given computational network to this network.
    /// `input_refs` says how to handle source nodes and must therefore have the the same length as the number of source nodes in `comp_net`.
    /// If the value in `input_refs` corresponding to a source node is `None`, that source node is added to the network.
    /// Otherwise, it is identified with whatever node is referenced.
    ///
    /// Returns a vector of references to the nodes the source nodes were turned into and a vector of references to the nodes the operator nodes were turned into.
    pub(crate) fn add_comp_net<A>(
        &mut self,
        comp_net: CompNetBuilder,
        input_refs: A,
    ) -> (Vec<NodeRefAny>, Vec<OperatorRef>)
    where
        A: AsRef<[Option<NodeRefAny>]>,
    {
        let input_refs = input_refs.as_ref();
        assert_eq!(comp_net.sources.len(), input_refs.as_ref().len());
        let new_source_refs: Vec<NodeRefAny> = comp_net
            .sources
            .into_iter()
            .zip(input_refs.iter())
            .map(|(source_node, &input_ref)| {
                if let Some(input_ref) = input_ref {
                    assert_eq!(source_node.dims(), self.node_dims(input_ref));
                    input_ref
                } else {
                    self.add_source_int(
                        source_node.value().clone(),
                        source_node.name().map(|s| s.to_owned()),
                    )
                    .as_any()
                }
            })
            .collect();
        let mut new_operator_refs: Vec<OperatorRef> = Vec::with_capacity(comp_net.operators.len());
        for (operator, operator_children) in comp_net.operators {
            let children: Vec<_> = operator_children
                .into_iter()
                .map(|node_ref| match node_ref.node_type() {
                    NodeType::Source => new_source_refs[node_ref.index()],
                    NodeType::Operator => new_operator_refs[node_ref.index()].as_any(),
                })
                .collect();
            let new_operator_ref = self.add_operator_int(operator, children);
            new_operator_refs.push(new_operator_ref);
        }
        (new_source_refs, new_operator_refs)
    }

    pub fn build(self) -> CompNet {
        CompNet::new(self.sources, self.operators)
    }

    pub fn build_and_children(self) -> (CompNet, Vec<SourceRef>) {
        let children: Vec<_> = (0..self.sources.len())
            .map(|i| SourceRef { index: i })
            .collect();
        (self.build(), children)
    }

    pub fn to_graph(&self) -> JsonValue {
        let mut result = JsonValue::new_object();
        let mut sources = JsonValue::new_array();
        for (index, source) in self.sources.iter().enumerate() {
            sources
                .push(
                    source
                        .name()
                        .map(|name| name.to_string())
                        .unwrap_or_else(|| format!("S{index}")),
                )
                .unwrap();
        }
        result["sources"] = sources;

        let mut operators = JsonValue::new_array();
        for (index, (operator, children)) in self.operators.iter().enumerate() {
            let mut operator_json = JsonValue::new_object();
            operator_json["name"] = operator
                .name()
                .map(|name| name.to_string())
                .unwrap_or_else(|| format!("O{index}"))
                .into();
            let mut children_json = JsonValue::new_array();
            for &child in children.iter() {
                children_json.push(self.node_name_or_index(child)).unwrap();
            }
            operator_json["children"] = children_json;
            operator_json["type"] = operator.funcs().name().into();
            if let Some(derivative_info) = operator.derivative_info() {
                let mut derivative_info_json = JsonValue::new_object();
                derivative_info_json["target"] =
                    self.node_name_or_index(derivative_info.target()).into();
                derivative_info_json["wrt"] = self.node_name_or_index(derivative_info.wrt()).into();
                operator_json["derivative_info"] = derivative_info_json;
            }
            operators.push(operator_json).unwrap();
        }
        result["operators"] = operators;
        result
    }
}

impl Default for CompNetBuilder {
    fn default() -> Self {
        Self::new()
    }
}
