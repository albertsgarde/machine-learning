use rand::Rng;

use crate::Vector;

pub fn generate_random_input_in_place(rng: &mut impl Rng, input: &mut Vector) {
    for i in 0..input.len() {
        input[i] = rng.gen_range(0. ..1.);
    }
}

pub fn generate_random_input(rng: &mut impl Rng, input_num: usize) -> Vector {
    Vector::from(
        (0..input_num)
            .map(|_| rng.gen_range(0. ..1.))
            .collect::<Vec<_>>(),
    )
}
