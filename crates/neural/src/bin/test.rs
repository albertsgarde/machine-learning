use neural::{
    comp_net::{CompNetBuilder, OperatorAdd, OperatorDotProduct, OperatorElementSum},
    Matrix,
};

fn main() {
    let mut builder = CompNetBuilder::new();
    let x = builder.add_source_with_name(Matrix::from_elem((1, 1), 4.), "x");
    let a = builder.add_source_with_name(Matrix::from_elem((1, 1), 2.), "a");
    let b = builder.add_source_with_name(Matrix::from_elem((1, 1), 7.), "b");
    let u = builder.add_operator_with_name(OperatorAdd, &[a, x], "u");
    let v = builder.add_operator_with_name(OperatorAdd, &[x, b], "v");
    let y = builder.add_operator_with_name(OperatorDotProduct, &[u, v], "y");
    let z = builder.add_operator_with_name(OperatorElementSum, &[y], "z");

    println!("{}", builder.to_graph().dump());

    let z_derivatives = builder.differentiate_with_names(
        z,
        (0, 0),
        vec![(x, Some("dx/dz")), (a, Some("da/dz")), (b, Some("db/dz"))],
    );

    println!("{}", builder.to_graph().dump());

    let mut net = builder.build();

    for &derivative in z_derivatives.iter() {
        net.compute(derivative);
    }

    /*
    let a = builder.add_source_with_name(
        Matrix::from_shape_vec((2, 2), vec![1., 2., 3., 4.]).unwrap(),
        "a",
    );
    let b = builder.add_source_with_name(
        Matrix::from_shape_vec((2, 2), vec![5., 6., 7., 8.]).unwrap(),
        "b",
    );
    let f = builder.add_operator_with_name(OperatorDotProduct, vec![a.as_any(), b.as_any()], "f");
    let g = builder.add_operator_with_name(OperatorIdentity, vec![f.as_any()], "g");
    let dzda = builder.differentiate_single_with_name(g, (1, 1), a, "dzda");

    let mut comp_net = builder.build();
    comp_net.compute(dzda);
    let dzda_value = comp_net.node_value(dzda);
    let a_value = comp_net.source_value(a);
    let b_value = comp_net.source_value(b);
    println!("{a_value:?}");
    println!("{b_value:?}");
    println!("{:?}", dzda_value);*/
}
