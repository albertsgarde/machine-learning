use crate::Vector;

pub trait DataIterator: Iterator<Item = (Vector, Vector)> + Clone {}

impl<T> DataIterator for T where T: Iterator<Item = (Vector, Vector)> + Clone {}

#[derive(Clone)]
pub struct DataIteratorFn<Rng, F>
where
    Rng: rand::Rng,
    F: FnMut(&mut Rng) -> (Vector, Vector),
{
    rng: Rng,
    f: F,
}

impl<Rng, F> DataIteratorFn<Rng, F>
where
    Rng: rand::Rng,
    F: FnMut(&mut Rng) -> (Vector, Vector),
{
    pub fn new(rng: Rng, f: F) -> Self {
        Self { rng, f }
    }
}

impl<Rng, F> Iterator for DataIteratorFn<Rng, F>
where
    Rng: rand::Rng,
    F: FnMut(&mut Rng) -> (Vector, Vector),
{
    type Item = (Vector, Vector);

    fn next(&mut self) -> Option<Self::Item> {
        Some((self.f)(&mut self.rng))
    }
}
