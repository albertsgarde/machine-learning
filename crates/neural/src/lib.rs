#![allow(clippy::module_inception)]
pub mod bin_utils;
pub mod comp_net;
pub mod utils;

use ndarray::{Array1, Array2};

pub type Vector = Array1<f32>;

pub type Matrix = Array2<f32>;

pub fn cost_function(output: &Vector, expected_output: &Vector) -> f32 {
    -(expected_output.t().dot(&output.map(|x| x.ln()))
        + (1. - expected_output)
            .t()
            .dot(&(1. - output.map(|x| x.ln()))))
}
