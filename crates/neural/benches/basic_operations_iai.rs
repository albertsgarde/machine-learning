use iai::black_box;
use neural::Matrix;
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;

const SEED: u64 = 0;

fn dot_bench(size: usize, repetitions: usize) {
    let mut rng = ChaCha8Rng::seed_from_u64(SEED);
    let a = Matrix::from_shape_simple_fn((size, 1), || rng.gen_range(0. ..1.));
    let b = Matrix::from_shape_simple_fn((1, size), || rng.gen_range(0. ..1.));
    black_box(a.dot(&b));
    black_box(Matrix::from_shape_simple_fn((size, size), || 1.));
    for _ in 0..repetitions {
        black_box(a.dot(&b));
    }
}

fn clone_bench(size: usize, repetitions: usize) {
    let mut rng = ChaCha8Rng::seed_from_u64(SEED);
    let a = Matrix::from_shape_simple_fn((size, 1), || rng.gen_range(0. ..1.));
    let b = Matrix::from_shape_simple_fn((1, size), || rng.gen_range(0. ..1.));
    black_box(a.dot(&b));
    let c = black_box(Matrix::from_shape_simple_fn((size, size), || 1.));
    for _ in 0..repetitions {
        black_box(c.clone());
    }
}

fn create_bench(size: usize, repetitions: usize) {
    let mut rng = ChaCha8Rng::seed_from_u64(SEED);
    let a = Matrix::from_shape_simple_fn((size, 1), || rng.gen_range(0. ..1.));
    let b = Matrix::from_shape_simple_fn((1, size), || rng.gen_range(0. ..1.));
    black_box(a.dot(&b));
    black_box(Matrix::from_shape_simple_fn((size, size), || 1.));
    for _ in 0..repetitions {
        black_box(Matrix::from_shape_simple_fn((size, size), || 1.));
    }
}

macro_rules! bench_size {
    ($name:ident, $size:expr, $repetitions:expr) => {
        mod $name {
            use super::*;
            fn dot() {
                dot_bench($size, $repetitions);
            }
            fn clone() {
                clone_bench($size, $repetitions);
            }
            fn create() {
                create_bench($size, $repetitions);
            }
            pub fn benchmarks() -> &'static [&'static (&'static str, fn())] {
                &[
                    &(
                        concat!(
                            stringify!($name),
                            " reps: ",
                            stringify!($repetitions),
                            " - dot"
                        ),
                        dot,
                    ),
                    &(
                        concat!(
                            stringify!($name),
                            " reps: ",
                            stringify!($repetitions),
                            " - clone"
                        ),
                        clone,
                    ),
                    &(
                        concat!(
                            stringify!($name),
                            " reps: ",
                            stringify!($repetitions),
                            " - create"
                        ),
                        create,
                    ),
                ]
            }
        }
    };
}
bench_size!(small, 2, 10000);
bench_size!(medium, 30, 10000);
bench_size!(large, 300, 1000);

fn main() {
    let mut benchmarks: Vec<&(&'static str, fn())> = vec![];
    benchmarks.extend_from_slice(small::benchmarks());
    benchmarks.extend_from_slice(medium::benchmarks());
    benchmarks.extend_from_slice(large::benchmarks());

    ::iai::runner(&benchmarks);
}
