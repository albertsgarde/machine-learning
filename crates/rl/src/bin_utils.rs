use std::hash::Hash;

use rand::SeedableRng;
use rand_chacha::ChaCha8Rng;

use super::{Environment, Episode, MaxValueAction, MonteCarlo, State};

#[allow(clippy::type_complexity)]
pub fn test_mc<E, V>(
    mc: &MonteCarlo<E, V>,
    test_num: usize,
    print_num: usize,
    seed: u64,
) -> (f32, Vec<Episode<E::State, E::Action>>)
where
    E: Environment,
    E::State: Hash + Eq,
    V: MaxValueAction<Environment = E>,
{
    let mut rng = ChaCha8Rng::seed_from_u64(seed);
    let runs = (0..test_num.max(print_num))
        .map(|_| {
            let start_state = E::State::sample_start(&mut rng);
            mc.run(&mut rng, start_state, None)
        })
        .collect::<Vec<_>>();
    let avg_score: f32 = runs
        .iter()
        .take(test_num)
        .map(|run| run.total_score())
        .sum::<f32>()
        / (test_num as f32);
    (
        avg_score,
        runs.into_iter().take(print_num).collect::<Vec<_>>(),
    )
}
