use std::{collections::HashMap, hash::Hash};

use conv::ApproxInto;

use super::MaxValueAction;
use crate::{Action, Environment, Episode, State};

pub struct MonteCarlo<E, V>
where
    E: Environment,
    E::State: Hash + Eq,
    V: MaxValueAction<Environment = E>,
{
    n0: f32,
    value_function: V,
    state_visit_count: HashMap<E::State, u32>,
    environment: E,
}

impl<E, V> MonteCarlo<E, V>
where
    E: Environment,
    E::State: Hash + Eq,
    V: MaxValueAction<Environment = E>,
{
    pub fn new(rng: &mut impl rand::Rng, environment: E, n0: u32) -> Self {
        Self {
            n0: n0.approx_into().unwrap(),
            value_function: V::initialize(rng),
            state_visit_count: HashMap::new(),
            environment,
        }
    }

    fn state_count(&self, state: &E::State) -> u32 {
        self.state_visit_count.get(state).copied().unwrap_or(0)
    }

    fn increment_state_count(&mut self, state: &E::State) {
        *self.state_visit_count.entry(state.clone()).or_insert(0) += 1;
    }

    fn epsilon(&self, state: &E::State) -> f32 {
        let num_prev_visits: f32 = self.state_count(state).approx_into().unwrap();
        self.n0 / (self.n0 + num_prev_visits)
    }

    pub fn sample_action(&self, rng: &mut impl rand::Rng, state: &E::State) -> E::Action {
        assert!(
            !state.is_terminal(),
            "Can't sample actions for terminal states."
        );
        let epsilon = self.epsilon(state);
        if rng.gen_bool(epsilon.into()) {
            E::Action::sample(rng)
        } else {
            self.value_function
                .max_value_action(state)
                .unwrap_or_else(|| E::Action::sample(rng))
        }
    }

    pub fn run(
        &self,
        rng: &mut impl rand::Rng,
        start_state: E::State,
        max_steps: Option<u32>,
    ) -> Episode<E::State, E::Action> {
        let mut episode = Episode::new(start_state);
        let mut num_steps = 0;
        while !episode.final_state().is_terminal()
            && max_steps.map_or(true, |max_steps| num_steps < max_steps)
        {
            num_steps += 1;
            let action = self.sample_action(rng, episode.final_state());
            let (reward, new_state) = self.environment.sample(rng, episode.final_state(), &action);
            episode.add(action, reward, new_state);
        }
        episode
    }

    pub fn train_single(
        &mut self,
        rng: &mut impl rand::Rng,
        max_steps: Option<u32>,
        gamma: f32,
    ) -> Episode<E::State, E::Action> {
        let state = E::State::sample_start(rng);
        let episode = self.run(rng, state, max_steps);
        let mut total_reward = 0.;
        for (state, action, reward) in episode.steps().iter().rev() {
            total_reward *= gamma;
            total_reward += reward;
            self.value_function.update(state, action, total_reward);
            self.increment_state_count(state);
        }
        episode
    }
}
