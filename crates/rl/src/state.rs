pub trait State: Clone {
    fn sample_start(rng: &mut impl rand::Rng) -> Self;

    fn is_terminal(&self) -> bool;
}
