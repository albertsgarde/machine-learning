use std::{collections::HashMap, hash::Hash};

use conv::ApproxInto;
use rand::Rng;

use crate::{Action, Environment, Episode, MaxValueAction, State};

#[derive(Clone, Debug)]
pub struct TemporalDifference<E, V>
where
    E: Environment,
    E::State: Hash + Eq,
    V: MaxValueAction<Environment = E>,
{
    value_function: V,
    environment: E,
    n0: f32,
    state_visit_count: HashMap<E::State, u32>,
}

impl<E, V> TemporalDifference<E, V>
where
    E: Environment,
    E::State: Hash + Eq,
    V: MaxValueAction<Environment = E>,
{
    pub fn new(rng: &mut impl Rng, environment: E, n0: u32) -> Self {
        Self {
            n0: n0.approx_into().unwrap(),
            value_function: V::initialize(rng),
            environment,
            state_visit_count: HashMap::new(),
        }
    }

    fn state_visit_count(&self, state: &E::State) -> u32 {
        self.state_visit_count.get(state).copied().unwrap_or(0)
    }

    fn increment_state_count(&mut self, state: &E::State) {
        *self.state_visit_count.entry(state.clone()).or_insert(0) += 1;
    }

    fn epsilon(&self, state: &E::State) -> f32 {
        let num_prev_visits: f32 = self.state_visit_count(state).approx_into().unwrap();
        self.n0 / (self.n0 + num_prev_visits)
    }

    fn sample_action(&self, rng: &mut impl rand::Rng, state: &E::State) -> E::Action {
        assert!(
            !state.is_terminal(),
            "Can't sample actions for terminal states."
        );
        let epsilon = self.epsilon(state);
        if rng.gen_bool(epsilon.into()) {
            E::Action::sample(rng)
        } else {
            self.value_function
                .max_value_action(state)
                .unwrap_or_else(|| E::Action::sample(rng))
        }
    }

    pub fn run_learn(
        &mut self,
        rng: &mut impl rand::Rng,
        start_state: E::State,
        max_steps: Option<u32>,
        gamma: f32,
    ) -> Episode<E::State, E::Action> {
        let mut episode = Episode::new(start_state);
        let mut num_steps = 0;
        while !episode.final_state().is_terminal()
            && max_steps.map_or(true, |max_steps| num_steps < max_steps)
        {
            num_steps += 1;
            let cur_state = episode.final_state();
            let action = self.sample_action(rng, cur_state);
            self.increment_state_count(cur_state);

            if let Some((prev_state, prev_action, prev_reward)) = episode.previous_step(0) {
                let predicted_total_reward = prev_reward
                    + self.value_function.value(cur_state, &action).unwrap_or(0.) * gamma;
                self.value_function
                    .update(prev_state, prev_action, predicted_total_reward);
            }

            let (reward, new_state) = self.environment.sample(rng, cur_state, &action);

            episode.add(action, reward, new_state);
        }
        if let Some((prev_state, prev_action, prev_reward)) = episode.previous_step(0) {
            self.value_function
                .update(prev_state, prev_action, *prev_reward);
        }
        episode
    }

    pub fn run(
        &self,
        rng: &mut impl rand::Rng,
        start_state: E::State,
        max_steps: Option<u32>,
    ) -> Episode<E::State, E::Action> {
        let mut episode = Episode::new(start_state);
        let mut num_steps = 0;
        while !episode.final_state().is_terminal()
            && max_steps.map_or(true, |max_steps| num_steps < max_steps)
        {
            num_steps += 1;
            let cur_state = episode.final_state();
            let action = self.sample_action(rng, episode.final_state());
            let (reward, new_state) = self.environment.sample(rng, cur_state, &action);
            episode.add(action, reward, new_state);
        }
        episode
    }

    pub fn train_single(
        &mut self,
        rng: &mut impl rand::Rng,
        max_steps: Option<u32>,
        gamma: f32,
    ) -> Episode<E::State, E::Action> {
        let state = E::State::sample_start(rng);
        self.run_learn(rng, state, max_steps, gamma)
    }
}
