pub trait Action: Clone {
    fn sample(rng: &mut impl rand::Rng) -> Self;
}
