use super::Environment;

type State<V> = <<V as ValueFunction>::Environment as Environment>::State;
type Action<V> = <<V as ValueFunction>::Environment as Environment>::Action;

pub trait ValueFunction {
    type Environment: Environment;

    fn initialize(rng: &mut impl rand::Rng) -> Self;

    fn value(&self, state: &State<Self>, action: &Action<Self>) -> Option<f32>;

    fn update(&mut self, state: &State<Self>, action: &Action<Self>, value: f32);
}

pub trait MaxValueAction: ValueFunction {
    fn max_value_action(&self, state: &State<Self>) -> Option<Action<Self>>;
}
