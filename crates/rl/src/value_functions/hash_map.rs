use std::{collections::HashMap, hash::Hash};

use conv::ApproxInto;

use crate::{Environment, MaxValueAction, ValueFunction};

#[derive(Clone, Debug)]
pub struct HashMapValueFunction<E>
where
    E: Environment,
    E::State: Eq + Hash,
    E::Action: Eq + Hash,
{
    value_function: HashMap<E::State, HashMap<E::Action, f32>>,
    state_action_count: HashMap<E::State, HashMap<E::Action, u32>>,
}

impl<E> HashMapValueFunction<E>
where
    E: Environment,
    E::State: Eq + Hash,
    E::Action: Eq + Hash,
{
    pub fn new() -> Self {
        HashMapValueFunction {
            value_function: HashMap::new(),
            state_action_count: HashMap::new(),
        }
    }

    fn state_action_count(&self, state: &E::State, action: &E::Action) -> u32 {
        self.state_action_count
            .get(state)
            .and_then(|map| map.get(action))
            .copied()
            .unwrap_or(0)
    }

    fn increment_state_action_count(&mut self, state: &E::State, action: &E::Action) {
        *self
            .state_action_count
            .entry(state.clone())
            .or_insert_with(HashMap::new)
            .entry(action.clone())
            .or_insert(0) += 1;
    }

    fn value_mut(&mut self, state: &E::State, action: &E::Action) -> &mut f32 {
        self.value_function
            .entry(state.clone())
            .or_insert_with(HashMap::new)
            .entry(action.clone())
            .or_insert(f32::NAN)
    }
}

impl<E> ValueFunction for HashMapValueFunction<E>
where
    E: Environment,
    E::State: Eq + Hash,
    E::Action: Eq + Hash,
{
    type Environment = E;

    fn initialize(_: &mut impl rand::Rng) -> Self {
        Self::new()
    }

    fn value(&self, state: &E::State, action: &E::Action) -> Option<f32> {
        self.value_function.get(state)?.get(action).copied()
    }

    fn update(&mut self, state: &E::State, action: &E::Action, reward: f32) {
        self.increment_state_action_count(state, action);
        let action_count: f32 = self
            .state_action_count(state, action)
            .approx_into()
            .unwrap();
        let cur_value = self.value_mut(state, action);
        if cur_value.is_nan() {
            assert!(action_count == 1.);
            *cur_value = reward;
        } else {
            assert!(action_count > 1.);
            *cur_value += (reward - *cur_value) / action_count;
        }
        assert_ne!(*cur_value, f32::NAN)
    }
}

impl<E> MaxValueAction for HashMapValueFunction<E>
where
    E: Environment,
    E::State: Eq + Hash,
    E::Action: Eq + Hash,
{
    fn max_value_action(&self, state: &E::State) -> Option<E::Action> {
        self.value_function
            .get(state)?
            .iter()
            .max_by(|&(_, value1), &(_, value2)| value1.partial_cmp(value2).unwrap())
            .map(|(action, _)| action.clone())
    }
}

impl<E> Default for HashMapValueFunction<E>
where
    E: Environment,
    E::State: Eq + Hash,
    E::Action: Eq + Hash,
{
    fn default() -> Self {
        Self::new()
    }
}
