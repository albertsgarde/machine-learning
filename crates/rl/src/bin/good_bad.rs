use conv::ApproxInto;
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha8Rng;
use rl::{
    good_bad::{Environment, State},
    value_functions::HashMapValueFunction,
    MonteCarlo, State as RlState, TemporalDifference,
};

const TEST_RUNS: u32 = 100000;

fn test_mc(
    rng: &mut impl Rng,
    mc: &MonteCarlo<Environment, HashMapValueFunction<Environment>>,
) -> f32 {
    let runs = TEST_RUNS;
    let mut total_reward = 0.;
    for _ in 0..runs {
        let start_state = State::sample_start(rng);
        total_reward += mc.run(rng, start_state, None).total_score();
    }
    let runs_f: f32 = runs.approx_into().unwrap();
    total_reward / runs_f
}

fn test_td(
    rng: &mut impl Rng,
    td: &TemporalDifference<Environment, HashMapValueFunction<Environment>>,
) -> f32 {
    let runs = TEST_RUNS;
    let mut total_reward = 0.;
    for _ in 0..runs {
        let start_state = State::sample_start(rng);
        total_reward += td.run(rng, start_state, None).total_score();
    }
    let runs_f: f32 = runs.approx_into().unwrap();
    total_reward / runs_f
}

fn main() {
    let training_batches = 10;
    let training_batch_size = 100;
    let gamma = 1.;

    let mut rng = ChaCha8Rng::seed_from_u64(3);

    let mut mc = MonteCarlo::<_, HashMapValueFunction<_>>::new(&mut rng, Environment {}, 100);
    let mut td =
        TemporalDifference::<_, HashMapValueFunction<_>>::new(&mut rng, Environment {}, 100);

    println!("MC initial score: {}", test_mc(&mut rng, &mc));

    println!("Training MC...");
    for batch in 1..=training_batches {
        for _ in 0..training_batch_size {
            mc.train_single(&mut rng, None, gamma);
        }
        println!("MC score after batch {batch}: {}", test_mc(&mut rng, &mc));
    }
    println!("TD initial score: {}", test_td(&mut rng, &td));
    println!("Training TD...");
    for batch in 1..=training_batches {
        for _ in 0..training_batch_size {
            td.train_single(&mut rng, None, gamma);
        }
        println!("TD score after batch {batch}: {}", test_td(&mut rng, &td));
    }
}
