use conv::ApproxInto;

use super::Environment;
use crate::{
    easy21::environment::{Action, State, StateFunction},
    MaxValueAction, ValueFunction,
};

pub struct Easy21ValueFunction {
    value_function: StateFunction<(f32, f32)>,
    state_action_count: StateFunction<(u32, u32)>,
}

impl Easy21ValueFunction {
    fn state_action_count(&mut self, state: State, action: Action) -> u32 {
        let (stick_count, hit_count) = self.state_action_count.get(state);
        match action {
            Action::Hit => hit_count,
            Action::Stick => stick_count,
        }
    }

    fn increment_state_action_count(&mut self, state: State, action: Action) {
        let (stick_count, hit_count) = self.state_action_count.get_mut(state);
        match action {
            Action::Hit => *hit_count += 1,
            Action::Stick => *stick_count += 1,
        }
    }

    fn value_mut(&mut self, state: State, action: Action) -> &mut f32 {
        let (stick_value, hit_value) = self.value_function.get_mut(state);
        match action {
            Action::Hit => hit_value,
            Action::Stick => stick_value,
        }
    }
}

impl ValueFunction for Easy21ValueFunction {
    type Environment = Environment;

    fn initialize(_: &mut impl rand::Rng) -> Self {
        Self {
            value_function: StateFunction::new(|| (f32::NAN, f32::NAN)),
            state_action_count: StateFunction::new(|| (0, 0)),
        }
    }

    fn value(&self, state: &State, action: &Action) -> Option<f32> {
        let (stick_value, hit_value) = self.value_function.get(*state);
        Some(match action {
            Action::Hit => hit_value,
            Action::Stick => stick_value,
        })
    }

    fn update(&mut self, state: &State, action: &Action, reward: f32) {
        assert_ne!(reward, f32::NAN);
        self.increment_state_action_count(*state, *action);
        let action_count: f32 = self
            .state_action_count(*state, *action)
            .approx_into()
            .unwrap();
        let cur_value = self.value_mut(*state, *action);
        if cur_value.is_nan() {
            assert!(action_count == 1.);
            *cur_value = reward;
        } else {
            assert!(action_count > 1.);
            *cur_value += (reward - *cur_value) / action_count;
        }
        assert_ne!(*cur_value, f32::NAN)
    }
}

impl MaxValueAction for Easy21ValueFunction {
    fn max_value_action(&self, state: &State) -> Option<Action> {
        let (stick_value, hit_value) = self.value_function.get(*state);
        if stick_value >= hit_value {
            Some(Action::Stick)
        } else if hit_value >= stick_value {
            Some(Action::Hit)
        } else {
            None
        }
    }
}
