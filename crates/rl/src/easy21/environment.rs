use std::fmt::Debug;

use ndarray::{Array2, ArrayBase};

use crate::{Action as RlAction, State as RlState};

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Card {
    value: u8,
    red: bool,
}

impl Card {
    pub fn new(value: u8, red: bool) -> Self {
        assert!((1..=10).contains(&value));
        Card { value, red }
    }
    fn sample_card_value(rng: &mut impl rand::Rng) -> u8 {
        rng.gen_range(1..=10)
    }

    pub fn draw_black_card(rng: &mut impl rand::Rng) -> Card {
        let value = Self::sample_card_value(rng);
        Card::new(value, false)
    }

    pub fn draw_card(rng: &mut impl rand::Rng) -> Card {
        let value = Self::sample_card_value(rng);
        let colour = rng.gen_bool(1. / 3.);
        Card::new(value, colour)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum State {
    Terminal,
    Play { dealer_value: u8, player_sum: u8 },
}

impl RlState for State {
    fn sample_start(rng: &mut impl rand::Rng) -> State {
        let player_card = Card::draw_black_card(rng);
        let dealer_card = Card::draw_black_card(rng);
        State::Play {
            dealer_value: dealer_card.value,
            player_sum: player_card.value,
        }
    }
    fn is_terminal(&self) -> bool {
        match self {
            State::Play { .. } => false,
            State::Terminal => true,
        }
    }
}

#[derive(Debug)]
pub struct StateFunction<V>
where
    V: Debug,
{
    array: Array2<V>,
}

impl<V> StateFunction<V>
where
    V: Copy + Debug,
{
    pub fn new<F>(value: F) -> Self
    where
        F: FnMut() -> V,
    {
        Self {
            array: ArrayBase::from_shape_simple_fn((21, 10), value),
        }
    }

    pub fn get(&self, state: State) -> V {
        if let State::Play {
            dealer_value,
            player_sum,
        } = state
        {
            *self
                .array
                .get((player_sum as usize - 1, dealer_value as usize - 1))
                .unwrap()
        } else {
            panic!("State functions do not take terminal states as inputs.")
        }
    }

    pub fn get_mut(&mut self, state: State) -> &mut V {
        if let State::Play {
            dealer_value,
            player_sum,
        } = state
        {
            self.array
                .get_mut((player_sum as usize - 1, dealer_value as usize - 1))
                .unwrap()
        } else {
            panic!("State functions do not take terminal states as inputs.")
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Action {
    Hit,
    Stick,
}

impl RlAction for Action {
    fn sample(rng: &mut impl rand::Rng) -> Action {
        if rng.gen_bool(0.5) {
            Action::Hit
        } else {
            Action::Stick
        }
    }
}

pub struct Environment {}

impl Environment {
    pub fn sample(rng: &mut impl rand::Rng, state: &State, action: &Action) -> (f32, State) {
        if let State::Play {
            dealer_value,
            player_sum,
        } = state
        {
            let player_sum = *player_sum;
            let dealer_value = *dealer_value;
            match action {
                Action::Hit => {
                    let card = Card::draw_card(rng);
                    if card.red {
                        if card.value >= player_sum {
                            (-1., State::Terminal)
                        } else {
                            (
                                0.,
                                State::Play {
                                    dealer_value,
                                    player_sum: player_sum - card.value,
                                },
                            )
                        }
                    } else {
                        let player_sum = player_sum + card.value;
                        if player_sum > 21 {
                            (-1., State::Terminal)
                        } else {
                            (
                                0.,
                                State::Play {
                                    dealer_value,
                                    player_sum,
                                },
                            )
                        }
                    }
                }
                Action::Stick => {
                    let mut dealer_value = dealer_value;
                    while (1..17).contains(&dealer_value) {
                        let card = Card::draw_card(rng);
                        if card.red {
                            if dealer_value <= card.value {
                                return (1., State::Terminal);
                            } else {
                                dealer_value -= card.value;
                            }
                        } else {
                            dealer_value += card.value;
                        }
                    }
                    let score = if dealer_value > 21 || player_sum > dealer_value {
                        1.
                    } else if dealer_value == player_sum {
                        0.
                    } else {
                        -1.
                    };
                    (score, State::Terminal)
                }
            }
        } else {
            panic!("No point in sampling when in terminal state.");
        }
    }
}

impl crate::Environment for Environment {
    type State = State;
    type Action = Action;

    fn sample(
        &self,
        rng: &mut impl rand::Rng,
        state: &Self::State,
        action: &Self::Action,
    ) -> (f32, Self::State) {
        Self::sample(rng, state, action)
    }
}
