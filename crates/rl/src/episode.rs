use std::fmt::Debug;

#[derive(Clone, Debug)]
pub struct Episode<S, A>
where
    S: Clone,
    A: Clone,
{
    steps: Vec<(S, A, f32)>,
    final_state: S,
}

impl<S, A> Episode<S, A>
where
    S: Clone,
    A: Clone,
{
    pub fn new(state: S) -> Self {
        Episode {
            steps: vec![],
            final_state: state,
        }
    }

    pub fn add(&mut self, action: A, reward: f32, mut new_state: S) {
        std::mem::swap(&mut new_state, &mut self.final_state);
        self.steps.push((new_state, action, reward));
    }

    pub fn steps(&self) -> &Vec<(S, A, f32)> {
        &self.steps
    }

    pub fn final_state(&self) -> &S {
        &self.final_state
    }

    pub fn previous_step(&self, n: usize) -> Option<&(S, A, f32)> {
        if self.steps.len() > n {
            self.steps.get(self.steps.len() - 1 - n)
        } else {
            None
        }
    }

    pub fn total_score(&self) -> f32 {
        self.steps.iter().map(|(_, _, score)| score).sum()
    }
}
