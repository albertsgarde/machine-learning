use crate::{Action as RlAction, State as RlState};

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum State {
    Start,
    Left,
    Right,
    End,
}

impl RlState for State {
    fn sample_start(_: &mut impl rand::Rng) -> State {
        State::Start
    }

    fn is_terminal(&self) -> bool {
        matches!(self, State::End)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Action {
    Left,
    Right,
}

impl RlAction for Action {
    fn sample(rng: &mut impl rand::Rng) -> Action {
        if rng.gen_bool(0.5) {
            Action::Left
        } else {
            Action::Right
        }
    }
}

#[derive(Debug, Clone)]
pub struct Environment {}

impl Environment {}

impl crate::Environment for Environment {
    type State = State;

    type Action = Action;

    fn sample(
        &self,
        rng: &mut impl rand::Rng,
        state: &Self::State,
        action: &Self::Action,
    ) -> (f32, Self::State) {
        match state {
            State::Start => match action {
                Action::Left => (0., State::Left),
                Action::Right => (if rng.gen_bool(0.01) { -200. } else { 1. }, State::Right),
            },
            State::Left => match action {
                Action::Left => (0., State::End),
                Action::Right => (0., State::End),
            },
            State::Right => match action {
                Action::Left => (0., State::End),
                Action::Right => (0., State::End),
            },
            State::End => panic!("No point sampling when in terminal state."),
        }
    }
}
