use rand::Rng;

use crate::{Action, State};

pub trait Environment {
    type State: State;
    type Action: Action;

    fn sample(
        &self,
        rng: &mut impl Rng,
        state: &Self::State,
        action: &Self::Action,
    ) -> (f32, Self::State);
}
